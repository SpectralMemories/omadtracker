// InputModal.js
import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { translate } from './i18n';

const ActivityModal = ({ isVisible, onClose, onSubmit, lang, unit}) => {
  const [input, setInput] = useState('');

  const handleSubmit = () => {
    onSubmit(input);
    setInput('');
  };

  let unitStr = translate(unit, lang);

  return (
    <Modal isVisible={isVisible}>
      <View style={styles.modalContent}>
        <Text>{translate('activity_prompt', lang).replaceAll('%%', unitStr)}</Text>
        <TextInput
          style={styles.input}
          value={input}
          onChangeText={setInput}
          keyboardType='numeric'
        />
        <View style={styles.buttonWrapper}>
          <TouchableOpacity style={{backgroundColor: '#ddd', padding: 8}} onPress={onClose}>
            <Text>{translate('close', lang)}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{backgroundColor: 'black', padding: 8}} onPress={handleSubmit}>
            <Text style={{color: 'white'}}>{translate('submit', lang)}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
  },
  input: {
    borderColor: '#ccc',
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  buttonWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    gap: 20,
    marginTop: 20
  }
});

export default ActivityModal;
