import en from './locales/en.json';
import fr from './locales/fr.json';

const locales = { en, fr };

export function translate (slug, locale = 'en') {
  return locales[locale][slug] || locales['en'][slug] || slug;
}
