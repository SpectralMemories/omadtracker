// InputModal.js
import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { translate } from './i18n';

const CaloriesModal = ({ isVisible, onClose, onSubmit, lang, difficulty}) => {
  const [input, setInput] = useState('');
  const [currentCost, setCurrentCost] = useState(0);

  const handleSubmit = () => {
    onSubmit(input);
    setInput('');
  };

  function onInputChanged (data) {
    data = parseInt(data);
    let points = -Math.ceil(data / 100.0);
    points = getPriceAdj(points, difficulty);

    setCurrentCost(Math.abs(points));
    setInput(data);
  }

  function getPriceAdj (price, diff2) {
    price = parseInt(price);
    let diffP;
    switch (parseInt(diff2)){
      case 1:
        diffP = -20;
        break;
      case 2:
        diffP = -10;
        break;
      case 4:
        diffP = 10;
        break;
      case 5:
        diffP = 20;
        break;
      default:
        diffP = 0;
        break;
    }
  
    
    diffP = diffP / 100.0;
    
    let diff = price * diffP;
    if(price > 0) diff *= -1;
    
    if(price > 0){
      return Math.max(0, Math.round(price + diff));
    }else if(price < 0){
      return Math.min(0, Math.round(price + diff));
    }
    
    return 0;
  }

  return (
    <Modal isVisible={isVisible}>
      <View style={styles.modalContent}>
        <Text>{translate('calories_prompt', lang)}</Text>
        <Text>{translate('cost', lang)}{currentCost}</Text>
        <TextInput
          style={styles.input}
          value={input}
          onChangeText={onInputChanged}
          keyboardType='numeric'
        />
        <View style={styles.buttonWrapper}>
          <TouchableOpacity style={{backgroundColor: '#ddd', padding: 8}} onPress={onClose}>
            <Text>{translate('close', lang)}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{backgroundColor: 'black', padding: 8}} onPress={handleSubmit}>
            <Text style={{color: 'white'}}>{translate('submit', lang)}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
  },
  input: {
    borderColor: '#ccc',
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  buttonWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    gap: 20,
    marginTop: 20
  }

  
});

export default CaloriesModal;
