import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Button, ToastAndroid, Text, Alert, TouchableOpacity, Image, AppState } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Updates from 'expo-updates';

import { translate } from './i18n';
import CaloriesModal from './CaloriesModal';
import ActivityModal from './ActivityModal';
import Slider from '@react-native-community/slider';
import { writeLog, readLog } from './PointsLog';

export default function App() {
  const DEBUG = false;

  const [currentPoints, setCurrentPoints] = useState(0);
  const [lastTimerStart, setLastTimerStart] = useState(null);
  const [timerRunning, setTimerRunning] = useState(false);
  const [remainingTime, setRemainingTime] = useState(0);
  const [caloriesModalVisible, setCaloriesModalVisible] = useState(false);
  const [activityModalVisible, setActivityModalVisible] = useState(false);
  const [appState, setAppState] = useState(AppState.currentState);

  const [isCheatDay, setIsCheatDay] = useState(false);
  const [isNoOmad, setIsNoOmad] = useState(false);
  const [isDoOmad, setIsDoOmad] = useState(false);
  const [isCheatMeal, setIsCheatMeal] = useState(false);
  const [isSkipMeal, setIsSkipMeal] = useState(false);

  const [isLoseWeight, setIsLoseWeight] = useState(true);
  const [difficulty, setDifficulty] = useState(3);
  const [lang, setLang] = useState('en');
  const [activityUnit, setActivityUnit] = useState('');

  const eatingWindow = 1800000;
  const maxPoints = 80;
  
  const caloriesPerPoint = 100;
  const pointLogMaxLines = 10;

  let addPointsOptions = [
    { calsPerUnit: 66, activity: 'walk', unit: 'unit_km', icon: require('./assets/icons/Walk_white.png'), label: 'walk' },
    { calsPerUnit: 3.33, activity: 'lift', unit: 'unit_minute', icon: require('./assets/icons/Gym_white.png'), label: 'lift' },
    { calsPerUnit: 78, activity: 'run', unit: 'unit_km', icon: require('./assets/icons/Run_white.png'), label: 'run' },
    { calsPerUnit: 9.16, activity: 'swim', unit: 'unit_minute', icon: require('./assets/icons/Swim_white.png'), label: 'swim' },
    { calsPerUnit: 30, activity: 'cycling', unit: 'unit_km', icon: require('./assets/icons/Cycling_white.png'), label: 'cycling' },
    { calsPerUnit: 1, activity: 'burn', unit: 'unit_calories', icon: require('./assets/icons/Burn_white.png'), label: 'burn' },
    { points: 10, perk: 'DoOmad', icon: require('./assets/icons/AllDayEating_white.png'), label: 'do_omad' },
    { points: 15, maintenanceAdj: 10, perk: 'SkipMeal', icon: require('./assets/icons/Fast_white.png'), label: 'skip_meal' }
  ];
  let subtractPointsOptions = [
    { points: -15, perk: 'CheatMeal', icon: require('./assets/icons/CheatMeal_white.png'), label: 'cheat_meal' },
    { points: -12, perk: 'NoOmad', icon: require('./assets/icons/AllDayEating_white.png'), label: 'skip_omad' },
    { points: -35, maintenanceAdj: 10, perk: 'CheatDay', icon: require('./assets/icons/CheatDay_white.png'), label: 'cheat_day' },
    { points: 0, perk: 'OtherFood', icon: require('./assets/icons/Sweets_white.png'), label: 'snack' }
  ];

  useEffect(() => {
    loadData();
    const handleAppStateChange = (nextAppState) => {
      if (appState.match(/inactive|background/) && nextAppState === 'active') {
        reloadApp();
      }
      setAppState(nextAppState);
    };

    const subscription = AppState.addEventListener('change', handleAppStateChange);

    return () => {
      clearInterval(global.timerId);
      subscription.remove();
    };
  }, [appState]);

  async function loadData() {
    let storedLang = await AsyncStorage.getItem('lang');
    setLocale(storedLang ? storedLang : 'en');

    const storedData = await AsyncStorage.getItem('pointsData');
    const pointsData = storedData ? JSON.parse(storedData) : { lastUpdated: null, points: 0, lastTimerStart: null };

    const today = getLocalDate().toISOString().slice(0, 10); // Today's date in YYYY-MM-DD format
    const lastUpdated = pointsData.lastUpdated ? new Date(pointsData.lastUpdated) : getLocalDate();
    const daysDifference = calculateDaysDifference(lastUpdated, new Date(today));

    let storedDifficulty = await AsyncStorage.getItem('difficulty');
    if(!storedDifficulty){
      storedDifficulty = 3;
    }
    changeDifficulty(parseInt(storedDifficulty));

    let pointsPerDay = getPointsPerDayAdj(parseInt(storedDifficulty));
    if (pointsPerDay > 0 && daysDifference > 0) {
      pointsData.points += daysDifference * pointsPerDay;
      if(DEBUG) console.log('adding ' + daysDifference + ' days worth of points');
      await AsyncStorage.setItem('pointsData', JSON.stringify(pointsData));

      if((daysDifference * pointsPerDay) != 0){
        global.lastReason = translate('daily_point_allowance', lang);
        await logPointChange((daysDifference * pointsPerDay));
      }
    }
    setCurrentPoints(pointsData.points);

    const now = getLocalDate();
    const lastTimerStart = pointsData.lastTimerStart ? new Date(pointsData.lastTimerStart) : null;
    const timerRunning = lastTimerStart && (now - lastTimerStart < getEatingWindowAdg(storedDifficulty)); // 3600000 ms in an hour

    if (timerRunning) {
        // Calculate remaining time
        const remainingTime = getEatingWindowAdg(storedDifficulty) - (now - lastTimerStart);
        setTimeout(notifyTimerEnd, remainingTime);

        setRemainingTime(Math.round(remainingTime / 1000));
        global.timerId = setInterval(() => {
          setRemainingTime((prevTime) => {
              if (prevTime <= 1) {
                  clearInterval(global.timerId);
                  global.timerId = null;
                  return 0;
              }
              return prevTime - 1;
          });
      }, 1000);
    }
    setTimerRunning(timerRunning);
    setLastTimerStart(lastTimerStart);

    //Active Perks
    const storedCheatMeal = await AsyncStorage.getItem('lastCheatMealStored');
    const storedCheatDay = await AsyncStorage.getItem('lastCheatDayStored');
    const storedNoOmad = await AsyncStorage.getItem('lastNoOmadStored');
    const storedDoOmad = await AsyncStorage.getItem('lastDoOmadStored');
    const storedSkipMeal = await AsyncStorage.getItem('lastSkipMealStored');

    let cheatDay = calcIsInPerk(storedCheatDay);
    setIsCheatDay(cheatDay);
    setIsNoOmad(cheatDay || calcIsInPerk(storedNoOmad));
    setIsCheatMeal(cheatDay || calcIsInPerk(storedCheatMeal));
    setIsSkipMeal(calcIsInPerk(storedSkipMeal));
    setIsDoOmad(calcIsInPerk(storedDoOmad));
    
    global.lastOpened = today;
    if(DEBUG) console.log('[INIT] points set to ' + pointsData.points);
    await AsyncStorage.setItem('pointsData', JSON.stringify({lastTimerStart: lastTimerStart, points: pointsData.points, lastUpdated: today }));
    global.realTimePoints = pointsData.points;

    global.actionStack = [];
}

function setLocale (locale){
  AsyncStorage.setItem('lang', locale);
  setLang(locale);
}

function getPriceAdj (price) {
  price = parseInt(price);
  let diffP;
  switch (parseInt(difficulty)){
    case 1:
      diffP = -20;
      break;
    case 2:
      diffP = -10;
      break;
    case 4:
      diffP = 10;
      break;
    case 5:
      diffP = 20;
      break;
    default:
      diffP = 0;
      break;
  }

  
  diffP = diffP / 100.0;
  
  let diff = price * diffP;
  if(price > 0) diff *= -1;
  
  if(price > 0){
    return Math.max(0, Math.round(price + diff));
  }else if(price < 0){
    return Math.min(0, Math.round(price + diff));
  }
  
  return 0;
}

function getEatingWindowAdg (diff) {
  switch (parseInt(diff)){
    case 0:
    case 1:
      return eatingWindow * 2;
    case 2:
    case 3:
    case 4:
      return eatingWindow * 1.5;
    case 5:
      return eatingWindow;
  }
}

function getPointsPerDayAdj (diff) {
  switch (parseInt(diff)){
    case 0:
      return 0;
    default:
      return (5 - diff);
  }
}

function getLocalDate () {
  const today = new Date();
  const timezoneOffset = today.getTimezoneOffset() * 60000; // offset in milliseconds

  return new Date(today - timezoneOffset);
}

async function modifyPoints(amount) {
  amount = getPriceAdj(amount);

  if(isLoseWeight){
    if (amount < 0 && Math.abs(amount) > currentPoints){
      Alert.alert(translate('no_points_head', lang), translate('no_points_body', lang));
      return false;
    }
  }else{
    if (amount < 0 && (Math.abs(amount) - maxPoints) > currentPoints){
      Alert.alert(translate('no_points_head', lang), translate('no_points_body', lang));
      return false;
    }
  }


  if (amount > 0 && currentPoints + amount > maxPoints){
    Alert.alert(translate('max_points_head', lang), translate('max_points_body', lang) + " (" + maxPoints + ")");
  }

  const newPoints = Math.min(currentPoints + amount, maxPoints);
  setCurrentPoints(newPoints);
  if(DEBUG) console.log('[ModifyPoints] points set to ' + newPoints);
  await AsyncStorage.setItem('pointsData', JSON.stringify({lastTimerStart: lastTimerStart, points: newPoints, lastUpdated: global.lastOpened }));

  global.realTimePoints = newPoints;
  await logPointChange(amount);
  return true;
}

async function logPointChange (change){
  const now = getLocalDate();
  let today = now.toISOString().replaceAll('T', ' ').split('.');
  today = today[0];

  let log = await readLog();
  let lines = log.split("\n");
  let polarity = (change >= 0 ? '+' : '');

  let reason = global.lastReason;
  if(reason){
    reason = " (" + reason + ")";
  }

  let newLine = "[" + today + "] " + polarity + change + reason;
  lines.unshift(newLine);

  if(lines.length > pointLogMaxLines){
    for(let i = 0; i < (lines.length - pointLogMaxLines); i++){
      lines.pop();
    }
  }
  await writeLog(lines.join("\n"));
}

function calcIsInPerk (timestamp) {
  const now = getLocalDate();
  const today = now.toISOString().slice(0, 10);

  return (timestamp && new Date(timestamp).toISOString().slice(0, 10) === today);
}

function formatTime(seconds) {
  const minutes = Math.floor(seconds / 60);
  const remainingSeconds = seconds % 60;
  return `${pad(minutes)}:${pad(remainingSeconds)}`;
}

function pad(time) {
  return time.toString().padStart(2, '0');
}

async function stopTimer () {
  if(global.timerId){
    clearInterval(global.timerId);
    global.timerId = null;
  }
  setTimerRunning(false);
  if(DEBUG) console.log('[StopTimer] points set to ' + global.realTimePoints);
  await AsyncStorage.setItem('pointsData', JSON.stringify({lastTimerStart: null, points: global.realTimePoints, lastUpdated: global.lastOpened }));
}

async function startTimer(resume) {
  const now = getLocalDate();
  const today = now.toISOString().slice(0, 10);
  const canStartNewTimer = !lastTimerStart || new Date(lastTimerStart).toISOString().slice(0, 10) !== today;

  resume = (resume === true);
  if(!resume){
    if(isCheatDay || isNoOmad || (!isLoseWeight && !isDoOmad)){
      return;
    }
  }
  
  if (canStartNewTimer || resume) {
      if(!resume) setLastTimerStart(now);
      setTimerRunning(true);

      if(!resume){
        global.actionStack.push({
          name: '',
          amount: 0,
          action: () => {
            setLastTimerStart(null);
            stopTimer();
          }
        });
      }

      let remainingTime;
      if(!resume){
        remainingTime = getEatingWindowAdg(difficulty);
      }else{
        remainingTime = getEatingWindowAdg(difficulty) - (now - lastTimerStart);
      }
      setRemainingTime(Math.round(remainingTime / 1000));
      setTimeout(notifyTimerEnd, getEatingWindowAdg(difficulty)); // 1 hour in milliseconds

      global.timerId = setInterval(() => {
            setRemainingTime((prevTime) => {
                if (prevTime <= 1) {
                    clearInterval(global.timerId);
                    global.timerId = null;
                    return 0;
                }
                return prevTime - 1;
            });
        }, 1000);
      if(DEBUG) console.log('[StartTimer] points set to ' + global.realTimePoints);
      await AsyncStorage.setItem('pointsData', JSON.stringify({lastTimerStart: now, points: global.realTimePoints, lastUpdated: global.lastOpened }));
  } else {
      Alert.alert(translate('already_ate_head', lang), translate('already_ate_body', lang));
  }
}

function notifyTimerEnd() {
  setTimerRunning(false);
}

  async function reset () {
    if(DEBUG) console.log('[Reset] points set to 0');
    await AsyncStorage.setItem('pointsData', JSON.stringify({ lastUpdated: null, points: 0, lastTimerStart: null }));
    await AsyncStorage.setItem('lastNoOmadStored', '');
    await AsyncStorage.setItem('lastCheatDayStored', '');
    await AsyncStorage.setItem('lastCheatMealStored', '');
    await AsyncStorage.setItem('lastSkipMealStored', '');
    await AsyncStorage.setItem('lastDoOmadStored', '');
    await AsyncStorage.setItem('difficulty', '');
    await writeLog('');
    reloadApp();
  }

  async function reloadApp() {
    try {
      await Updates.reloadAsync();
    } catch (e) {
      console.error("Failed to reload the app", e);
    }
  }
  
  function calculateDaysDifference(startDate, endDate) {
    const msPerDay = 24 * 60 * 60 * 1000;
    return Math.floor((endDate - startDate) / msPerDay);
  }

  async function handleActivityAnswer (amount){
    let item = global.lastItem;

    let pointsRaw = (item.calsPerUnit / 100) * amount;
    let points = Math.floor(pointsRaw);

    if(pointsRaw - points >= 0.8){
      points++;
    }

    if(points > 0){
      await modifyPoints(points);
      global.actionStack.push({
        name: global.lastReason,
        amount: points
      });
      toast(translate('positive_toast', lang));
    }

    setActivityModalVisible(false);
  }

  async function handleOtherFoodAnswer (cals){
    let points = Math.ceil(cals / caloriesPerPoint);

    if(cals > 0 && await modifyPoints(-points)){
      global.actionStack.push({
        name: global.lastReason,
        amount: -points
      });
      toast(translate('negative_toast', lang));
    }
    
    setCaloriesModalVisible(false);
  }

  async function onButtonPress (item){
    global.lastReason = translate(item.label, lang);
    global.lastItem = item;

    let halt = false;

    if(item.perk === 'OtherFood'){
      //Show popup
      setCaloriesModalVisible(true);

      halt = true;
    }

    if(item.activity){
      setActivityUnit(item.unit);
      setActivityModalVisible(true);

      halt = true;
    }

    let undoAction = {name: global.lastReason, amount: 0};
    if(item.points && !halt){
      let finalPoints = item.points;
      if(!isLoseWeight && item.maintenanceAdj){
        finalPoints += item.maintenanceAdj;
      }
      if(!await modifyPoints(finalPoints)){
        return;
      }
      undoAction.amount = finalPoints;
    }

    if(item.perk && !halt){
      const now = getLocalDate();
      const today = now.toISOString().slice(0, 10);
      switch (item.perk) {
        case 'DoOmad':
          setIsDoOmad(true);

          undoAction.action = () => {
            setIsDoOmad(false);
            stopTimer();
          };
          await AsyncStorage.setItem('lastDoOmadStored', today);
          break;
        case 'NoOmad':
          setIsNoOmad(true);
          stopTimer();

          undoAction.action = () => {
            setIsNoOmad(false);
            startTimer(true);
          };

          await AsyncStorage.setItem('lastNoOmadStored', today);
          break;
        case 'CheatDay':
          setIsCheatDay(true);
          setIsCheatMeal(true);
          setIsNoOmad(true);
          stopTimer();

          undoAction.action = () => {
            setIsCheatDay(false);
            setIsCheatMeal(false);
            setIsNoOmad(false);
            startTimer(true);
          };
          await AsyncStorage.setItem('lastCheatDayStored', today);
          break;
        case 'CheatMeal':
          setIsCheatMeal(true);

          undoAction.action = () => {
            setIsCheatMeal(false);
          };
          await AsyncStorage.setItem('lastCheatMealStored', today);
          break;
        case 'SkipMeal':
          setIsSkipMeal(true);
          stopTimer();

          undoAction.action = () => {
            setIsSkipMeal(false);
            startTimer(true);
          };
          await AsyncStorage.setItem('lastSkipMealStored', today);
          break;
      }
    }

    if(!halt){
      if(item.points > 0){
        toast(translate('positive_toast', lang));
      }else if(item.points < 0){
        toast(translate('negative_toast', lang));
      }
      global.actionStack.push(undoAction);
    }
  }

  function timerText () {
    if(isSkipMeal){
      return translate('meal_skipped', lang);
    }

    if(isCheatDay){
      return translate('cheat_day_today', lang);
    }

    if(!isLoseWeight && !isDoOmad){
      if(isCheatMeal){
        return translate('maintenance_all_day_with_cheat', lang);
      }
      return translate('maintenance_all_day', lang)
    }

    if(isNoOmad){
      if(isCheatMeal){
        return translate('no_omad_with_cheat', lang);
      }
      return translate('no_omad', lang);
    }

    if (timerRunning){
      if(isCheatMeal){
        return translate('cheat_meal_active', lang);
      }
      return translate('eating_window_active', lang);
    }

    const now = getLocalDate();
    const today = now.toISOString().slice(0, 10);
    const canStartNewTimer = !lastTimerStart || new Date(lastTimerStart).toISOString().slice(0, 10) !== today;

    if (canStartNewTimer){
      if(isCheatMeal){
        return translate('start_cheat_meal', lang);
      }
      return translate('start_eating_window', lang);
    }
    
    return translate('already_ate', lang);
  }

  function timerStyle() {
    if(!isLoseWeight && !isDoOmad){
      return {
        color: '#42f569',
        fontWeight: 'bold'
      };
    }

    if(isSkipMeal){
      return {
        color: '#bbb',
        fontWeight: 'bold'
      };
    }

    if(isCheatDay){
      return {
        color: '#4287f5',
        fontWeight: 'bold'
      };
    }

    if(isNoOmad){
      return {
        color: '#42f569',
        fontWeight: 'bold'
      };
    }

    if (timerRunning){
      if(isCheatMeal){
        return {
          color: '#42f569'
        };
      }
      return {color: '#ddd'};
    }

    const now = getLocalDate();
    const today = now.toISOString().slice(0, 10);
    const canStartNewTimer = !lastTimerStart || new Date(lastTimerStart).toISOString().slice(0, 10) !== today;

    if (canStartNewTimer){
      return {
        backgroundColor: '#ddd',
        color: 'black',
        padding: 10,
      };
    }else{
      return {
        color: '#f54254'
      };
    }
  }

  function isPerkActive (perk){
    switch (perk) {
      case 'DoOmad':
        return isLoseWeight || isDoOmad || isSkipMeal || isCheatDay;
      case 'NoOmad':
        return !isLoseWeight || isNoOmad || isCheatDay || isSkipMeal;
      case 'CheatDay':
        return isCheatDay;
      case 'CheatMeal':
        return isCheatMeal || isSkipMeal || isCheatDay;
      case 'SkipMeal':
        return isSkipMeal || isCheatDay || isCheatMeal || isNoOmad || isDoOmad;
      case 'OtherFood':
        return isCheatDay;
    }

    return false;
  }

  function undo () {
    return; //TODO
    let lastAction = global.actionStack.pop();
    if(lastAction){
      global.lastReason = translate('undo_of', lang) + lastAction.name;
      if(Math.abs(lastAction.amount) > 0) modifyPoints(-lastAction.amount);
      if(lastAction.action){
        lastAction.action();
      }

      toast(translate('undone', lang));
    }
  }

  function isPerkVisible (perk){
    switch (perk) {
      case 'DoOmad':
        return !(isLoseWeight);
      case 'NoOmad':
        return isLoseWeight;
    }

    return true;
  }

  function toast (text){
    ToastAndroid.show(text, ToastAndroid.SHORT);
  }

  function getDebugButtons () {
    if(DEBUG){
      return (
        <>
        <IconButton icon={require('./assets/icons/info_white.png')} label="Reset" onPress={reset} />
        <IconButton icon={require('./assets/icons/info_white.png')} label="Reload" onPress={reloadApp} />
        <Text style={{color: '#fff'}}>DEBUG</Text>
        </>
      );
    }
    return '';
  }

  function showInfo() {
    Alert.alert(translate('info_window_title', lang), translate('info_window', lang));
  }

  function showTips () {
    Alert.alert(translate('omad_window_title', lang), translate('omad_window', lang));
  }

  async function showPointsLog () {
    Alert.alert(translate('points_log_window_title', lang), await readLog());
  }

  function buttonLabel (item) {
    let finalPoints = item.points;
    if(!isLoseWeight && item.maintenanceAdj){
      finalPoints += item.maintenanceAdj;
    }
    finalPoints = getPriceAdj(finalPoints);

    let prefix = (finalPoints > 0 ? '+' : '');
    let end = " [" + prefix + finalPoints + "]";

    if(item.perk == 'OtherFood' || item.activity){
      end = '';
    }

    return translate(item.label, lang) + end;
  }

  function IconButton({ icon, label, onPress, disabled, style }) {
    if(!disabled){
      disabled = false;
    }

    let imgStyle = {
      width: '100%',
      height: '100%',
      aspectRatio: 1,
      resizeMode: 'contain',
      opacity: 0.85
    };
    let textStyle = {color: '#ddd'};

    if(disabled){
      imgStyle.opacity = 0.1;
      textStyle.color = 'grey';
    }

    return (
      <TouchableOpacity style={[styles.button, style]} disabled={disabled} onPress={onPress}>
        <View style={{width: 40, height: 40, overflow: 'hidden'}}>
          <Image source={icon} style={imgStyle} />
        </View>
        <Text style={textStyle}>{label}</Text>
      </TouchableOpacity>
    );
  }

  async function changeDifficulty (diff) {
    setDifficulty(diff);
    setIsLoseWeight(diff > 0);
    
    await AsyncStorage.setItem('difficulty', diff.toString());
  }

  function getMaximumStr () {
    if(currentPoints < 0 && !isLoseWeight){
      return ' / -' + maxPoints;
    }
    return ' / ' + maxPoints;
  }
  function getPerDayStr () {
    return '+ ' + getPointsPerDayAdj(difficulty);
  }
  function getIntensityName () {
    return translate('difficulty-' + difficulty);
  }

  function renderActionButton (item, index){
    if(isPerkVisible(item.perk)){
      return (
        <IconButton key={index} disabled={isPerkActive(item.perk)} icon={item.icon} label={buttonLabel(item)} onPress={() => onButtonPress(item)} />
      );
    }
    return ('');
  }

  return (
    <View style={styles.container}>
      <Text style={styles.balance}>{translate('balance', lang)}{currentPoints}
        <Text style={{color: 'grey'}}>{getMaximumStr()}</Text>
      </Text>
      <Text style={{color: 'grey', fontSize: 16, marginBottom: 50}}>
        {getPerDayStr()} {translate('per_day', lang)}
      </Text>
      <View style={styles.buttonContainer}>
        {addPointsOptions.map((item, index) => 
          (renderActionButton(item, index))
        )}
      </View>
      <View style={styles.buttonContainer2}>
        {subtractPointsOptions.map((item, index) => 
          (renderActionButton(item, index))
        )}
      </View>
      <TouchableOpacity onPress={startTimer} disabled={timerRunning}>
          <Text style={timerStyle()}>{timerText()}</Text>
          <View>
              <Text style={{color: '#ddd', textAlign: 'center'}}>{timerRunning ? formatTime(remainingTime) : ""}</Text>
          </View>
      </TouchableOpacity>
      
      {getDebugButtons()}
      
      <Text style={styles.text}>{translate('mode_switch', lang)}{getIntensityName()}</Text>
      
      <Slider
        minimumValue={0}
        maximumValue={5}
        step={1}
        value={difficulty}
        onValueChange={changeDifficulty}
        style={{width: 200, height: 40}}
        minimumTrackTintColor={'#ffffff'}
        maximumTrackTintColor={'#ffffff'}
        thumbTintColor={'#ffffff'}
      />

      <CaloriesModal
        isVisible={caloriesModalVisible}
        onClose={() => {setCaloriesModalVisible(false)}}
        onSubmit={handleOtherFoodAnswer}
        lang={lang}
        difficulty={difficulty}
      />

      <ActivityModal
        isVisible={activityModalVisible}
        onClose={() => {setActivityModalVisible(false)}}
        onSubmit={handleActivityAnswer}
        lang={lang}
        unit={activityUnit}
      />
    
    <IconButton style={styles.undoButton} disabled={((global.actionStack ? global.actionStack.length : 0) <= 0) || !DEBUG} icon={require('./assets/icons/undo_white.png')} label="Undo" onPress={undo} />
    <IconButton style={styles.logButton} icon={require('./assets/icons/logs_white.png')} label={translate('logs', lang)} onPress={showPointsLog} />
    <IconButton style={styles.infoButton} icon={require('./assets/icons/info_white.png')} label={translate('info', lang)} onPress={showInfo} />
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#222',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    paddingVertical: 60,
    paddingHorizontal: 10,
  },
  balance: {
    fontSize: 24,
    marginTop: 10,
    color: '#ddd'
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap',
    width: '100%',
    marginBottom: 50,
    gap: 20
  },
  buttonContainer2: {
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap',
    width: '100%',
    marginBottom: 30,
    gap: 20
  },
  text: {
    color: '#ddd',
    textAlign: 'center'
  },
  button: {
    alignItems: 'center',
    padding: 0,
  },
  infoButton: {
    position: 'absolute',
    bottom: 10,
    right: 15
  },
  logButton: {
    position: 'absolute',
    bottom: 10,
    right: 75
  },
  undoButton: {
    display: 'none',
    position: 'absolute',
    bottom: 10,
    right: 135
  }
});
