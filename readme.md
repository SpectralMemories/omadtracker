# OMAD Tracker

## Overview

- [Google Play store link](https://gitlab.com/SpectralMemories/omadtracker)
- Apple App Store: TBD, maybe

This is the source code of my OMAD Tracker app. Its made using Expo and React Native

## Build instructions

- Install the Expo CLI on your machine
- Rename the app.sanitized.json file to app.json
- To build the APK for Android on my Linux computer, I use these:

> export ANDROID_HOME=/your/android/sdk
>
> echo 'y' | npx expo prebuild --clean
>
> eas build -p android --profile dev --local

## The code is trash

Yes, I know. This is my first ever Expo / React Native project, and this codebase is the result of trial and error trying to make it all work