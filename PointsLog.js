// fileUtils.js
import * as FileSystem from 'expo-file-system';

const fileUri = FileSystem.documentDirectory + 'pointlog.txt';

export const writeLog = async (content) => {
  try {
    await FileSystem.writeAsStringAsync(fileUri, content);
  } catch (e) {}
};

export const readLog = async () => {
  try {
    const content = await FileSystem.readAsStringAsync(fileUri);
    return content;
  } catch (e) {
    return '';
  }
};
